import compiler, inspect

def unsafe_access(obj, attr):
    return obj.attr

def safe_access(obj,attr):
    """
    So in the AST, we'll substitute any of these nodes:
    
    GetattrOrNone(
      Name('obj'),  # subnode 1
      'attr'        # subnode 2
    )

    By nodes looking like this:

    CallFunc(
      Name('__get_attr_or_None_dammit'),
      Name('obj'),     # subnode 1
      'attr',          # subnode 2
      None,
      None
    )
     

    And we'll define the __get_attr_or_None_dammit function, append
    it to the ast so it gets re-hydrated by Armin Rigo's    
    """
    try:
        return obj.attr
    except AttributeError:
        return None

def long_chain(obj,attr1, attr2, attr3, attr4, attr5, attr6):
    return obj.attr1[attr2].attr3[attr4][attr5].attr6

def assignment(a, b, obj, attr):
    a = 1
    b = obj.attr
    c = obj.attr['foo']
    obj[attr] = 'bar'

def comprehension(objs, attr):
    return [obj.attr for obj in objs]

def calling_functions(obj, attr):
   z = obj.attr
   return safe_access(unsafe_access(obj, attr), attr)

