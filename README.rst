=============================================================
question_dot: Safe Attribute Access Implementation for Python
=============================================================

Welcome to question_dot!

question_dot abuses the Pypy implementation, PEP 263 source encodings
and the patience of strangers in order to implement the `?.` operator,
called the "safe navigation" operator in languages like Groovy,
C# and Newspeak.

Here, we'll call it the Safe Attribute Access operator, because we want
to sound formal and academic. This is also why the sole author writes in
the first person plural.

Like its companion project `quiet_none`, this is an experiment in abusing 
the Python facilities in order to explore nicer syntax for safe attribute
access. It is most emphatically NOT FOR PRODUCTION.

If you really want a better syntax for conditional attribute access, 
please take a look at PEP 463 - Exception-catching expressions.

How to safely access attributes
===============================

TODO: Source encoding is still in the pipes. 

For now, all you can do is execute the modified pypy and use the `?.` 
operator "natively".

Check out the question_dot branch, and run the hosted interpreter::

    $ git checkout question_dot
    $ ./pypy/bin/pyinteractive.py

Once you're in the interpreter, you can just use the `?.` operator. It will return the requested attribute, or None if the attribute doesn't exist::

	PyPy 2.6.0-alpha0 in StdObjSpace on top of Python 2.7.6 (startuptime: 7.48 secs)
	>>>> 
	>>>> 'foo'.__len__
	<bound method str.__len__ of 'foo'>
	>>>> 'foo'.bar
	Traceback (application-level):
	  File "<inline>", line 1 in <module>
	    'foo'.bar
	AttributeError: 'str' object has no attribute 'bar'
	>>>> print 'foo'?.bar
	None
	>>>> print None?.None
	None
	>>>> 
	>>>> # Safe attribute access only works in a Loading context:
	>>>> 
	>>>> from collections import namedtuple
	>>>> Foo = namedtuple('Foo', ('x', 'y'))
	>>>> foo = Foo(1, 2)
	>>>> foo.x
	1
	>>>> print foo?.z
	None
	>>>> foo.x = 'bar'
	Traceback (application-level):
	  File "<inline>", line 1 in <module>
	    foo.x = 'bar'
	AttributeError: can't set attribute
	>>>> foo?.x = 3
	SyntaxError: illegal expression for assignment (line 1)
	>>>>


How this works
==============

The grammar, tokeniser, parser, ast and bytecode emitter of pypy have been modified to add the new operator. It's pretty standard.

Note, however, two details:

* The new operator/syntax has been implemented without adding any new bytecodes.
* The bytecode implementation almost certainly leaks memory by refusing to pop the block where the safe attribute access takes place. This is NOT for production.

More details to follow after source encoding has been implemented.

(To Be Continued...)

======================
BUILT WITH PYPY!!!!!!!
======================


PyPy is both an implementation of the Python programming language, and
an extensive compiler framework for dynamic language implementations.
You can build self-contained Python implementations which execute
independently from CPython.

The home page is:

    http://pypy.org/

If you want to help developing PyPy, this document might help you:

    http://doc.pypy.org/

It will also point you to the rest of the documentation which is generated
from files in the pypy/doc directory within the source repositories. Enjoy
and send us feedback!

    the pypy-dev team <pypy-dev@python.org>




=====================================
PyPy: Python in Python Implementation
=====================================

Welcome to PyPy!

PyPy is both an implementation of the Python programming language, and
an extensive compiler framework for dynamic language implementations.
You can build self-contained Python implementations which execute
independently from CPython.

The home page is:

    http://pypy.org/

If you want to help developing PyPy, this document might help you:

    http://doc.pypy.org/

It will also point you to the rest of the documentation which is generated
from files in the pypy/doc directory within the source repositories. Enjoy
and send us feedback!

    the pypy-dev team <pypy-dev@python.org>


Building
========

build with:

.. code-block:: console

    $ rpython/bin/rpython -Ojit pypy/goal/targetpypystandalone.py

This ends up with ``pypy-c`` binary in the main pypy directory. We suggest
to use virtualenv with the resulting pypy-c as the interpreter; you can
find more details about various installation schemes here:

    http://doc.pypy.org/en/latest/install.html
