# ______________________________________________________________________
"""Module pytokenize

THIS FILE WAS COPIED FROM pypy/module/parser/pytokenize.py AND ADAPTED
TO BE ANNOTABLE (Mainly made lists homogeneous)

This is a modified version of Ka-Ping Yee's tokenize module found in the
Python standard library.

The primary modification is the removal of the tokenizer's dependence on the
standard Python regular expression module, which is written in C.  The regular
expressions have been replaced with hand built DFA's using the
basil.util.automata module.

$Id: pytokenize.py,v 1.3 2003/10/03 16:31:53 jriehl Exp $
"""
# ______________________________________________________________________

from pypy.interpreter.pyparser import automata

__all__ = [ "tokenize" ]

# ______________________________________________________________________
# Automatically generated DFA's

accepts = [True, True, True, True, True, True, True, False,
           True, True, True, False, True, True, True, True,
           False, False, False, True, False, False, True,
           False, False, True, False, True, False, True,
           False, False, True, False, False, True, True,
           True, False, False, True, False, False, False,
           True]
states = [
    # 0
    {'\t': 0, '\n': 14, '\x0c': 0,
     '\r': 15, ' ': 0, '!': 11, '"': 17,
     '#': 19, '%': 13, '&': 13, "'": 16,
     '(': 14, ')': 14, '*': 8, '+': 13,
     ',': 14, '-': 13, '.': 6, '/': 12,
     '0': 4, '1': 5, '2': 5, '3': 5,
     '4': 5, '5': 5, '6': 5, '7': 5,
     '8': 5, '9': 5, ':': 14, ';': 14,
     '<': 10, '=': 13, '>': 9, '?': 7,
     '@': 14, 'A': 1, 'B': 2, 'C': 1,
     'D': 1, 'E': 1, 'F': 1, 'G': 1,
     'H': 1, 'I': 1, 'J': 1, 'K': 1,
     'L': 1, 'M': 1, 'N': 1, 'O': 1,
     'P': 1, 'Q': 1, 'R': 3, 'S': 1,
     'T': 1, 'U': 2, 'V': 1, 'W': 1,
     'X': 1, 'Y': 1, 'Z': 1, '[': 14,
     '\\': 18, ']': 14, '^': 13, '_': 1,
     '`': 14, 'a': 1, 'b': 2, 'c': 1,
     'd': 1, 'e': 1, 'f': 1, 'g': 1,
     'h': 1, 'i': 1, 'j': 1, 'k': 1,
     'l': 1, 'm': 1, 'n': 1, 'o': 1,
     'p': 1, 'q': 1, 'r': 3, 's': 1,
     't': 1, 'u': 2, 'v': 1, 'w': 1,
     'x': 1, 'y': 1, 'z': 1, '{': 14,
     '|': 13, '}': 14, '~': 14},
    # 1
    {'0': 1, '1': 1, '2': 1, '3': 1,
     '4': 1, '5': 1, '6': 1, '7': 1,
     '8': 1, '9': 1, 'A': 1, 'B': 1,
     'C': 1, 'D': 1, 'E': 1, 'F': 1,
     'G': 1, 'H': 1, 'I': 1, 'J': 1,
     'K': 1, 'L': 1, 'M': 1, 'N': 1,
     'O': 1, 'P': 1, 'Q': 1, 'R': 1,
     'S': 1, 'T': 1, 'U': 1, 'V': 1,
     'W': 1, 'X': 1, 'Y': 1, 'Z': 1,
     '_': 1, 'a': 1, 'b': 1, 'c': 1,
     'd': 1, 'e': 1, 'f': 1, 'g': 1,
     'h': 1, 'i': 1, 'j': 1, 'k': 1,
     'l': 1, 'm': 1, 'n': 1, 'o': 1,
     'p': 1, 'q': 1, 'r': 1, 's': 1,
     't': 1, 'u': 1, 'v': 1, 'w': 1,
     'x': 1, 'y': 1, 'z': 1},
    # 2
    {'"': 17, "'": 16, '0': 1, '1': 1,
     '2': 1, '3': 1, '4': 1, '5': 1,
     '6': 1, '7': 1, '8': 1, '9': 1,
     'A': 1, 'B': 1, 'C': 1, 'D': 1,
     'E': 1, 'F': 1, 'G': 1, 'H': 1,
     'I': 1, 'J': 1, 'K': 1, 'L': 1,
     'M': 1, 'N': 1, 'O': 1, 'P': 1,
     'Q': 1, 'R': 3, 'S': 1, 'T': 1,
     'U': 1, 'V': 1, 'W': 1, 'X': 1,
     'Y': 1, 'Z': 1, '_': 1, 'a': 1,
     'b': 1, 'c': 1, 'd': 1, 'e': 1,
     'f': 1, 'g': 1, 'h': 1, 'i': 1,
     'j': 1, 'k': 1, 'l': 1, 'm': 1,
     'n': 1, 'o': 1, 'p': 1, 'q': 1,
     'r': 3, 's': 1, 't': 1, 'u': 1,
     'v': 1, 'w': 1, 'x': 1, 'y': 1,
     'z': 1},
    # 3
    {'"': 17, "'": 16, '0': 1, '1': 1,
     '2': 1, '3': 1, '4': 1, '5': 1,
     '6': 1, '7': 1, '8': 1, '9': 1,
     'A': 1, 'B': 1, 'C': 1, 'D': 1,
     'E': 1, 'F': 1, 'G': 1, 'H': 1,
     'I': 1, 'J': 1, 'K': 1, 'L': 1,
     'M': 1, 'N': 1, 'O': 1, 'P': 1,
     'Q': 1, 'R': 1, 'S': 1, 'T': 1,
     'U': 1, 'V': 1, 'W': 1, 'X': 1,
     'Y': 1, 'Z': 1, '_': 1, 'a': 1,
     'b': 1, 'c': 1, 'd': 1, 'e': 1,
     'f': 1, 'g': 1, 'h': 1, 'i': 1,
     'j': 1, 'k': 1, 'l': 1, 'm': 1,
     'n': 1, 'o': 1, 'p': 1, 'q': 1,
     'r': 1, 's': 1, 't': 1, 'u': 1,
     'v': 1, 'w': 1, 'x': 1, 'y': 1,
     'z': 1},
    # 4
    {'.': 25, '0': 22, '1': 22, '2': 22,
     '3': 22, '4': 22, '5': 22, '6': 22,
     '7': 22, '8': 24, '9': 24, 'B': 23,
     'E': 26, 'J': 14, 'L': 14, 'O': 21,
     'X': 20, 'b': 23, 'e': 26, 'j': 14,
     'l': 14, 'o': 21, 'x': 20},
    # 5
    {'.': 25, '0': 5, '1': 5, '2': 5,
     '3': 5, '4': 5, '5': 5, '6': 5,
     '7': 5, '8': 5, '9': 5, 'E': 26,
     'J': 14, 'L': 14, 'e': 26, 'j': 14,
     'l': 14},
    # 6
    {'0': 27, '1': 27, '2': 27, '3': 27,
     '4': 27, '5': 27, '6': 27, '7': 27,
     '8': 27, '9': 27},
    # 7
    {'.': 14},
    # 8
    {'*': 13, '=': 14},
    # 9
    {'=': 14, '>': 13},
    # 10
    {'<': 13, '=': 14, '>': 14},
    # 11
    {'=': 14},
    # 12
    {'/': 13, '=': 14},
    # 13
    {'=': 14},
    # 14
    {},
    # 15
    {'\n': 14},
    # 16
    {automata.DEFAULT: 31, '\n': 28,
     '\r': 28, "'": 29, '\\': 30},
    # 17
    {automata.DEFAULT: 34, '\n': 28,
     '\r': 28, '"': 32, '\\': 33},
    # 18
    {'\n': 14, '\r': 15},
    # 19
    {automata.DEFAULT: 19, '\n': 28, '\r': 28},
    # 20
    {'0': 35, '1': 35, '2': 35, '3': 35,
     '4': 35, '5': 35, '6': 35, '7': 35,
     '8': 35, '9': 35, 'A': 35, 'B': 35,
     'C': 35, 'D': 35, 'E': 35, 'F': 35,
     'a': 35, 'b': 35, 'c': 35, 'd': 35,
     'e': 35, 'f': 35},
    # 21
    {'0': 36, '1': 36, '2': 36, '3': 36,
     '4': 36, '5': 36, '6': 36, '7': 36},
    # 22
    {'.': 25, '0': 22, '1': 22, '2': 22,
     '3': 22, '4': 22, '5': 22, '6': 22,
     '7': 22, '8': 24, '9': 24, 'E': 26,
     'J': 14, 'L': 14, 'e': 26, 'j': 14,
     'l': 14},
    # 23
    {'0': 37, '1': 37},
    # 24
    {'.': 25, '0': 24, '1': 24, '2': 24,
     '3': 24, '4': 24, '5': 24, '6': 24,
     '7': 24, '8': 24, '9': 24, 'E': 26,
     'J': 14, 'e': 26, 'j': 14},
    # 25
    {'0': 25, '1': 25, '2': 25, '3': 25,
     '4': 25, '5': 25, '6': 25, '7': 25,
     '8': 25, '9': 25, 'E': 38, 'J': 14,
     'e': 38, 'j': 14},
    # 26
    {'+': 39, '-': 39, '0': 40, '1': 40,
     '2': 40, '3': 40, '4': 40, '5': 40,
     '6': 40, '7': 40, '8': 40, '9': 40},
    # 27
    {'0': 27, '1': 27, '2': 27, '3': 27,
     '4': 27, '5': 27, '6': 27, '7': 27,
     '8': 27, '9': 27, 'E': 38, 'J': 14,
     'e': 38, 'j': 14},
    # 28
    {},
    # 29
    {"'": 14},
    # 30
    {automata.DEFAULT: 41, '\n': 14, '\r': 15},
    # 31
    {automata.DEFAULT: 31, '\n': 28,
     '\r': 28, "'": 14, '\\': 30},
    # 32
    {'"': 14},
    # 33
    {automata.DEFAULT: 42, '\n': 14, '\r': 15},
    # 34
    {automata.DEFAULT: 34, '\n': 28,
     '\r': 28, '"': 14, '\\': 33},
    # 35
    {'0': 35, '1': 35, '2': 35, '3': 35,
     '4': 35, '5': 35, '6': 35, '7': 35,
     '8': 35, '9': 35, 'A': 35, 'B': 35,
     'C': 35, 'D': 35, 'E': 35, 'F': 35,
     'L': 14, 'a': 35, 'b': 35, 'c': 35,
     'd': 35, 'e': 35, 'f': 35, 'l': 14},
    # 36
    {'0': 36, '1': 36, '2': 36, '3': 36,
     '4': 36, '5': 36, '6': 36, '7': 36,
     'L': 14, 'l': 14},
    # 37
    {'0': 37, '1': 37, 'L': 14, 'l': 14},
    # 38
    {'+': 43, '-': 43, '0': 44, '1': 44,
     '2': 44, '3': 44, '4': 44, '5': 44,
     '6': 44, '7': 44, '8': 44, '9': 44},
    # 39
    {'0': 40, '1': 40, '2': 40, '3': 40,
     '4': 40, '5': 40, '6': 40, '7': 40,
     '8': 40, '9': 40},
    # 40
    {'0': 40, '1': 40, '2': 40, '3': 40,
     '4': 40, '5': 40, '6': 40, '7': 40,
     '8': 40, '9': 40, 'J': 14, 'j': 14},
    # 41
    {automata.DEFAULT: 41, '\n': 28,
     '\r': 28, "'": 14, '\\': 30},
    # 42
    {automata.DEFAULT: 42, '\n': 28,
     '\r': 28, '"': 14, '\\': 33},
    # 43
    {'0': 44, '1': 44, '2': 44, '3': 44,
     '4': 44, '5': 44, '6': 44, '7': 44,
     '8': 44, '9': 44},
    # 44
    {'0': 44, '1': 44, '2': 44, '3': 44,
     '4': 44, '5': 44, '6': 44, '7': 44,
     '8': 44, '9': 44, 'J': 14, 'j': 14},
    ]
pseudoDFA = automata.DFA(states, accepts)

accepts = [False, False, False, False, False, True]
states = [
    # 0
    {automata.DEFAULT: 0, '"': 1, '\\': 2},
    # 1
    {automata.DEFAULT: 4, '"': 3, '\\': 2},
    # 2
    {automata.DEFAULT: 4},
    # 3
    {automata.DEFAULT: 4, '"': 5, '\\': 2},
    # 4
    {automata.DEFAULT: 4, '"': 1, '\\': 2},
    # 5
    {automata.DEFAULT: 4, '"': 5, '\\': 2},
    ]
double3DFA = automata.NonGreedyDFA(states, accepts)

accepts = [False, False, False, False, False, True]
states = [
    # 0
    {automata.DEFAULT: 0, "'": 1, '\\': 2},
    # 1
    {automata.DEFAULT: 4, "'": 3, '\\': 2},
    # 2
    {automata.DEFAULT: 4},
    # 3
    {automata.DEFAULT: 4, "'": 5, '\\': 2},
    # 4
    {automata.DEFAULT: 4, "'": 1, '\\': 2},
    # 5
    {automata.DEFAULT: 4, "'": 5, '\\': 2},
    ]
single3DFA = automata.NonGreedyDFA(states, accepts)

accepts = [False, True, False, False]
states = [
    # 0
    {automata.DEFAULT: 0, "'": 1, '\\': 2},
    # 1
    {},
    # 2
    {automata.DEFAULT: 3},
    # 3
    {automata.DEFAULT: 3, "'": 1, '\\': 2},
    ]
singleDFA = automata.DFA(states, accepts)

accepts = [False, True, False, False]
states = [
    # 0
    {automata.DEFAULT: 0, '"': 1, '\\': 2},
    # 1
    {},
    # 2
    {automata.DEFAULT: 3},
    # 3
    {automata.DEFAULT: 3, '"': 1, '\\': 2},
    ]
doubleDFA = automata.DFA(states, accepts)

#_______________________________________________________________________
# End of automatically generated DFA's

endDFAs = {"'" : singleDFA,
           '"' : doubleDFA,
           'r' : None,
           'R' : None,
           'u' : None,
           'U' : None,
           'b' : None,
           'B' : None}

for uniPrefix in ("", "u", "U", "b", "B"):
    for rawPrefix in ("", "r", "R"):
        prefix = uniPrefix + rawPrefix
        endDFAs[prefix + "'''"] = single3DFA
        endDFAs[prefix + '"""'] = double3DFA

whiteSpaceStatesAccepts = [True]
whiteSpaceStates = [{'\t': 0, ' ': 0, '\x0c': 0}]
whiteSpaceDFA = automata.DFA(whiteSpaceStates, whiteSpaceStatesAccepts)

# ______________________________________________________________________
# COPIED:

triple_quoted = {}
for t in ("'''", '"""',
          "r'''", 'r"""', "R'''", 'R"""',
          "u'''", 'u"""', "U'''", 'U"""',
          "b'''", 'b"""', "B'''", 'B"""',
          "ur'''", 'ur"""', "Ur'''", 'Ur"""',
          "uR'''", 'uR"""', "UR'''", 'UR"""',
          "br'''", 'br"""', "Br'''", 'Br"""',
          "bR'''", 'bR"""', "BR'''", 'BR"""'):
    triple_quoted[t] = t
single_quoted = {}
for t in ("'", '"',
          "r'", 'r"', "R'", 'R"',
          "u'", 'u"', "U'", 'U"',
          "b'", 'b"', "B'", 'B"',
          "ur'", 'ur"', "Ur'", 'Ur"',
          "uR'", 'uR"', "UR'", 'UR"',
          "br'", 'br"', "Br'", 'Br"',
          "bR'", 'bR"', "BR'", 'BR"'):
    single_quoted[t] = t

tabsize = 8

# PYPY MODIFICATION: removed TokenError class as it's not needed here

# PYPY MODIFICATION: removed StopTokenizing class as it's not needed here

# PYPY MODIFICATION: removed printtoken() as it's not needed here

# PYPY MODIFICATION: removed tokenize() as it's not needed here

# PYPY MODIFICATION: removed tokenize_loop() as it's not needed here

# PYPY MODIFICATION: removed generate_tokens() as it was copied / modified
#                    in pythonlexer.py

# PYPY MODIFICATION: removed main() as it's not needed here

# ______________________________________________________________________
# End of pytokenize.py

